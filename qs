#/bin/bash

# note:		must be run as . qs (alternatively can be run with "source")
# protip:	add alias qs='. qs' to ~/.bash_profile (aka ~/.bashrc)

if [ -z "$1" ]; then
	# jump to anchor (if set) and null anchor
	if [ ! -z "$qs_anchor" ]; then
		cd "$qs_anchor"
		qs_anchor=""
		echo "qs: returned to anchor"

	# if no jump destination set, set jump dest (first call)
	elif [ -z "$qs_jumpTo" ]; then
		qs_jumpTo="$(pwd)"
		echo "qs: set"

	# if anchor point not set and jump destination is set (general use case)
	else
		temp="$(pwd)"
		cd "$qs_jumpTo"
		qs_jumpTo="$temp"
	fi
else
	# help
	if [ "$1" == '-h' ]; then
	echo 'QuickSwitch (qs):'
		echo 'Switch back and forth between two directories.'
		echo 'First call to qs will set switch destination to current directory.'
		echo -e 'Subsequent calls will jump to switch dest and change dest to\n\tdirectory qs was called from.'
		echo -e 'Note:  must be run as . qs'
		echo 'Flags:'
		echo -e '\t-a: anchor point at current dir, next qs will jump here,\n\t\tdoes not alter switch dest'
		echo -e '\t-c: clears both switch loctions (switch dest and anchor)'
		echo -e '\t-h: help page'
		echo -e '\t-r: remove anchor point'
		echo -e '\t-s: set switch destination to current directory'
		echo -e '\t-v: view switch destination and anchor infromation'

	# display switch destination and anchor info
	elif [ "$1" == '-v' ]; then
		if [ ! -z "$qs_jumpTo" ]; then
			echo "Switch dest: $qs_jumpTo"
		fi
		if [ ! -z "$qs_anchor" ]; then
			echo "Anchored at: $qs_anchor"
		fi

	# set jump point to  be current working directory
	elif [ "$1" == '-s' ]; then
		qs_jumpTo="$(pwd)"
		echo "qs: set"

	# set anchor point to jump to on next qs
	elif [ "$1" == '-a' ]; then
		qs_anchor="$(pwd)"
		echo "qs: anchored"

	# remove anchor point
	elif [ "$1" == '-r' ]; then
		qs_anchor=""
		echo "qs: anchor removed"

	# remove jump point and anchor point
	elif [ "$1" == '-c' ]; then
		qs_jumpTo=""
		qs_anchor=""
		echo "qs: switch locations cleared"

	# invalid flag
	else
		echo "qs: invalid flag \"$1\""
	fi
fi


