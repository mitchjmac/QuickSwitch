# QuickSwitch
Allows a user to quickly cycle between two directories.

## Notes
- This script must be run as `. qs` in order to make the switch locations persistent. (In reality, it works by making the variables of the subprocess `qs` avaliable to the parent shell.) This document will refer to `. qs` as `qs`.
- It is suggested to add `alias qs='. qs'` to your `~/.bash_profile` or `~/.bashrc`.
- This document will refer to the location that calls to `qs` will jump to as **switch destination** and the location of an anchor point as **anchor destination.**

## Flags
\-a:	anchor qs at the current directory, the next `qs` call will jump to here  
\-c:	clears both switch loctions (*switch dest* and *anchor dest*)  
\-h:	help page  
\-r:	remove anchor point (next call to qs will now instead jump to *switch destination*)  
\-s:	set switch destination to current directory  
\-v:	view switch destination and anchor destination

## Usage
### qs
- The primary use case is when a user must often switch back and forth between the same two directories; these directiories will be referred to as the **main cycle**. (i.e. repeated calls to `qs` will simply jump back and forth between the same two directories.)
- QuickSwitch has no inherent concept of *main cycle*, it only knows *switch destination* and can read the current directory at the time of the call to `qs` (thus `cd`'ing will alter the main cycle)
- The first time `qs` is called, the only thing that will happen is that the *switch destination* will be set to the current directory.
- After the first call, subsequent calls to `qs` will `cd` to *switch destination* and then *switch destination* will be set to the directory `qs` was called from.  

### anchors
- Anchor points are set at the current directory using `qs -a`. When an anchor is set, `qs` will instead `cd` the user to *anchor destination*.
- The primary use case of anchor points is to temporarily leave the *main cycle*—to do work in another directory—without destroying the *main cycle*.

### workflow example
1. `cd` to the root of a git repo
2. Initialize QuickSwitch with `qs` (setting *switch destination* to `/path/to/repo/root`)
3. `cd` to a sub-directory in repo and do work there
4. `qs` to jump to `/path/to/repo/root` to commit/view to-do's/etc. (setting *switch destination* to `/path/to/repo/subdir`)
5. `qs` to jump back to sub-directory when finished in the root (setting *switch destination* to `/path/to/repo/root`)
6. while working, you need to briefly check something in `~`. `qs -a` to set an anchor at `/path/to/repo/subdir`
7. `cd ~`
8. when finished in `~`, `qs` to return back to the anchor at `/path/to/repo/subdir` (*switch destination* is still set to `/path/to/repo/root`)
9. `qs` to jump to repo root (setting *switch destination* to `/path/to/repo/subdir`)

### changing the 'main cycle'
- To completly destroy the main cycle and manually recreate it issue `qs -c`. To preserve part of the cycle, read below.
- Assume *main cycle* currently consists of your current directory `~/Documents` and the *switch destination* `~/Desktop`.
- You now want to change the *main cycle* to include `/usr/local/bin` and one of the directories in the existing *main cycle*.
- Assuming you `qs -a` while in `~/Documents`, then `cd /usr/local/bin`:
  1. `qs -r; qs` to set the *main cycle* to `~/Desktop` and `/usr/local/bin`
  2. `qs -s; qs` to set the *main cycle* to `~/Documents` and `/usr/local/bin`
- If no anchor was set before issuing `cd /usr/local/bin`:
  1. `qs` to set the *main cycle* to `~/Desktop` and `/usr/local/bin`
  2. `qs -s` then `cd ~/Documents` to set the *main cycle* to `~/Documents` and `/usr/local/bin`
